<?php
namespace Hansvandersluis\SearchApiClient;

use \Cache;
use \Log;

class SearchApiClient {

    /**
     * Search the term with the api
     *
     * @param $term
     * @return string
     */
    public function search($term){

        $searchResult= $this->callEnpoint('search', ['term'=>$term], 'api_search_result');
        return $searchResult;
    }

    /**
     * Send user feedback to the api
     *
     * @param $id
     * @return string
     */
    public function feedback($id){

        $result= $this->callEnpoint('feedback', ['feedback'=>$id], 'api_feedback_result');
        return $result;
    }

    /**
     * Search the term with the api
     *
     * @param $id
     * @param $data
     * @return string
     */
    public function update($id, $data){

        $parameters= ['id' => $id, 'data' => $data];
        $result= $this->callEnpoint('update', $parameters, 'api_update_result');
        return $result;
    }

    /**
     * Isuue the call to the endpoint, check the cache for existing data,
     * , check the request authentication, and also resets the authentication
     * status if needed
     *
     * @param string $endpoint endpoint name
     * @param array $parameters endpoint parameters
     * @param string $cachename name of the local cache for the endpoint result
     * @return string
     */
    public function callEnpoint($endpoint, $parameters, $cachename){
        $result=[];
        //fetch the data from the cache first
        if(Cache::has($cachename)){
            $content= Cache::get($cachename);
            $result= json_decode($content,true);
            $this->log('fetched search result from cache');
        }
        else{
            //when token is on cache
            if(Cache::has('api_token')){
                $result= $this->getEndpointResult($endpoint, $parameters);

                //when the result says "token invalid", reset api_token on
                //cache and call the endpoint again
                if($result == 'token_invalid'){
                    Cache::forget('api_token');
                    $result= $this->callEnpoint($endpoint, $parameters, $cachename);
                }
                else{
                    //save result to cache
                    $this->log('Saving search result to cache');
                    Cache::put($cachename, json_encode($result), SearchApiConf::$cacheLifetime);
                }
            }
            else{
                //authenticate when token doesn't exist
                if($this->authenticate()){
                    //try to call the endpoint again with a new token
                    $result= $this->callEnpoint($endpoint, $parameters, $cachename);
                }
                else{
                    //authentication is not posible
                    $result="AUTHENTICATION_ERROR";
                }
            }

        }
        return $result;
    }

    /**
     * Connect to the api endpoint and get the data
     *
     * @param string $endpoint endpoint name
     * @param array $parameters endpoint parameters
     * @param string $cachename name of the local cache for the endpoint result
     *
     * @return string the enpoint content or a string when the token is invalid
     */
    public function getEndpointResult($endpoint, $parameters, $method='get'){

        $endpointParameters='';
        foreach($parameters as $paramKey => $paramData){
            $endpointParameters.= '/'.$paramData;
        }
        $result= $this->connect($endpoint.$endpointParameters, $method);

        $endpointResult= json_decode($result['content'],true);

        if($endpointResult['content'] == 'token_invalid'){
            $endpointResult= 'token_invalid';
            $this->log('searching with invalid token');
        }
        else if($endpointResult['content'] == 'token_expired'){
            $endpointResult= 'token_invalid';
            $this->log('searching with invalid token');
        }
        else if($endpointResult['content'] == 'token_absent'){
            $endpointResult= 'token_invalid';
            $this->log('searching with invalid token');
        }
        else{
            $this->log('Problems searching the api');
        }

        return $endpointResult;
    }


    /**
     * authenticate with the api and save the token to cache
     */
    public function authenticate(){

        $result=false;

        //set the post fields using the SearchApiConf values
        $postFields= [
            'email' => SearchApiConf::$email,
            'password' => SearchApiConf::$password,
        ];

        //send authentication request
        $response= $this->connect('auth','get',$postFields);
        $response= json_decode($response['content']);

        //when the authentication is correct the token is received
        if(isset($response->token)){
            $this->log('Authentication - success');
            //save to api_token on cache
            Cache::forever('api_token', $response->token);
            $this->log('Authentication - token saved on cache');
            $result=true;
        }
        else{
            //reset api key
            Cache::forget('api_token');
            $this->log('Authentication - failed : token removed from cache');
        }

        return $result;
    }

    /**
     * Connect to the novastyl service and retireve the given resource
     * @param type $resource
     * @param type $method http method
     */
    public function connect ($resource, $method='get', $parameters=[]){
        //get api token from cache
        $apiToken= Cache::get('api_token');

        //set resource's full url
        $url= SearchApiConf::$serviceBaseUrl. $resource;

        //add parameters to url on get method
        if($method == 'get'){
            $url.='?';
            foreach($parameters as $paramKey => $paramVal){
                $url.= $paramKey. '=' .$paramVal.'&';
            }
            $url= substr($url, 0, strlen($url)-1);
        }

        //set headers
        $headers=['User-Agent: curl/7.41.0'];
        if($method == 'post'){
            $headers[]= 'Content-Type: application/x-www-form-urlencoded';
        }
        else{
            $headers[]='Content-Type: text/plain; charset=UTF-8';
        }

        if(isset($apiToken) && $apiToken != ''){
            $headers[]= 'Authorization: Bearer '.$apiToken;
        }

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        if($method == 'post'){
            curl_setopt($ch, CURLOPT_POST, true);
            if(isset($parameters)){
                curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($parameters));
            }
        }
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_AUTOREFERER, 1);
        curl_setopt($ch, CURLOPT_FRESH_CONNECT, 1);
        //curl_setopt($ch, CURLINFO_HEADER_OUT, true);

        $result['content'] = curl_exec($ch);
        $result['status']= curl_getinfo($ch, CURLINFO_HTTP_CODE);

        //Log::info(curl_getinfo($ch));

        if($result['status'] != 200){
            $this->log('Api call:' . $result['content']);
        }

        $curlError= curl_error($ch);

        if($curlError != ''){
            $this->log('CURL_ERRORS: '. $curlError);
        }
        curl_close($ch);

        return $result;
    }

    public function log($message){
        if(SearchApiConf::$enableLog){
            Log::info('SEARCH API: '. $message);
        }
    }
}